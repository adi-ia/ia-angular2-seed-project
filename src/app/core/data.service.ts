import { identifierModuleUrl } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { ConfigService } from './config.service';

@Injectable()
export class DataService {
	private api: string;

	constructor(private http: HttpClientModule, private configService: ConfigService) {
		this.api = this.configService.apiUrl;
	}
}
